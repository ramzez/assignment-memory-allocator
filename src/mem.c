#include <stdarg.h>

#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size
size_from_capacity( block_capacity
cap );
extern inline block_capacity
capacity_from_size( block_size
sz );

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

// сколько страниц займет mem байт
static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

// сколько байт займут страницы (возможно полупустые)
static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

// инициализация блока в переменную addr
static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static struct region create_region(void *restrict addr, size_t sz, bool extends) {
    return (struct region) {
            .addr = addr,
            .size = sz,
            .extends = extends
    };
}

// если байты query вмещаются в один регион, то он будет REGION_MIN_SIZE, иначе больше
static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool

region_is_invalid(const struct region *r);

// выделить место в памяти размера length, начиная с адреса start
// при ошибке вернет -1
static void *map_pages(void const *start, size_t length, int additional_flags) {
    return mmap((void *) start, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
// addr - откуда начать аллоцировать
// query - размер данных в байтах
static struct region alloc_region(void const *addr, size_t query) {
    struct region reg;
    query = region_actual_size(query);
    //пробуем продолжить предыдущий регион флагом
    void *allocated_address = map_pages(addr, query, MAP_FIXED_NOREPLACE);
    //если не получилось, аллоцируем где попало
    if (allocated_address == MAP_FAILED) {
        allocated_address = map_pages(addr, query, 0);
        reg = create_region(allocated_address, query, false);
    } else
        reg = create_region(allocated_address, query, true);
    block_init(allocated_address, (block_size) {query}, NULL);
    return reg;
}

static void *block_after(struct block_header const *block);

// инициализировать регион в начале кучи, вернуть его адрес
void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;
    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */
// хватает ли байтов разделить данный блок хотя бы на два
static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free && query +
                             offsetof(
    struct block_header, contents ) +
                                            BLOCK_MIN_CAPACITY
                                    <= block->capacity.bytes;
}

// разделить на два если места хватает
static bool split_if_too_big(struct block_header *block, size_t query) {
    if (block_splittable(block, query)) {
        if (query <= BLOCK_MIN_CAPACITY)
            query = BLOCK_MIN_CAPACITY;

        block_size free_block_size = (block_size) {
                size_from_capacity(block->capacity).bytes -
                size_from_capacity((block_capacity) {query}).bytes
        };
        struct block_header *free_block = (void*)((uint8_t*) block + offsetof(struct block_header, contents) + query);
        block_init(free_block, free_block_size, NULL);

        block->capacity.bytes = query;
        block->next = free_block;
        return true;
    }
    return false;

}


/*  --- Слияние соседних свободных блоков --- */
// вернет адрес конца блока + 1
static void *block_after(const struct block_header *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

// лежат ли два блока вплотную
static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

// объеденяемы ли два блока (пустые + лежат вплотную)
static bool mergeable(const struct block_header *restrict fst, const struct block_header *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

// в цикле соединять блоки пока можно начиная с block
static bool try_merge_with_next(struct block_header *block) {
    struct block_header *next_block = block->next;
    if (next_block != NULL && mergeable(block, next_block)) {
        block->capacity.bytes += size_from_capacity(next_block->capacity).bytes;
        block->next = next_block->next;
        return true;
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */
struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};

// засунуть n байт в какой-нибудь свободный блок, вернуть этот блок в block
static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t n) {
    struct block_header *good_block = block;
    struct block_header *last_block = block;
    while (good_block != NULL) {
        while (try_merge_with_next(good_block));
        if (block_is_big_enough(n, good_block) && good_block->is_free)
            return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = good_block};
        last_block = good_block;
        good_block = good_block->next;
    }
    return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = last_block};

}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result free_block = find_good_or_last(block, query);
    split_if_too_big(free_block.block, query);
    return free_block;

}

// выделить новое место mmap'ом на query байт (проверить region_actual_size)
// пытаясь вплотную к блоку last
// связать новый блок с last'ом
// вернуть адрес нового блока (last->next)
static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    block_size size = size_from_capacity(last->capacity);

    void *new_addr = (void*)((uint8_t *) last + size.bytes);


    struct region new_region = alloc_region(new_addr, query);
    struct block_header *new_block = (struct block_header *) new_region.addr;
    last->next = new_block;
    if (try_merge_with_next(last)) {
        return last;
    } else {
        return last->next;
    }

}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result bsr = try_memalloc_existing(query, heap_start);
    switch (bsr.type) {
        case BSR_REACHED_END_NOT_FOUND:
            if ((bsr.block = grow_heap(bsr.block, query)) == NULL) {
                return NULL;
            }
            split_if_too_big(bsr.block, query);
            break;
        case BSR_FOUND_GOOD_BLOCK:
            break;
        default:
            return NULL;
    }
    bsr.block->is_free = false;
    return bsr.block;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(
    struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    if (!header->is_free) {
        header->is_free = true;
        while (try_merge_with_next(header));
    }
}